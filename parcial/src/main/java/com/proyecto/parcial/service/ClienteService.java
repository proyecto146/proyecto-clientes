package com.proyecto.parcial.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proyecto.parcial.model.Cliente;
import com.proyecto.parcial.repository.ClienteRepositorio;


@Service
public class ClienteService {
    
    @Autowired
    private ClienteRepositorio clienteRepositorio;

    public Cliente create (Cliente cliente){

        return clienteRepositorio.save(cliente);

    }

    public List<Cliente> getAllCliente() {
        return clienteRepositorio.findAll();
    }

    public String deleteCliente(Integer id){
        clienteRepositorio.deleteById(id);
        return "OK";
    };
    

    public Optional<Cliente> findById(Integer id) {
        return clienteRepositorio.findById(id);

    }

    public Cliente update (Cliente cliente){
        return clienteRepositorio.save(cliente);
    }

}
