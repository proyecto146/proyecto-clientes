package com.proyecto.parcial.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proyecto.parcial.model.Cliente;

public interface ClienteRepositorio extends JpaRepository<Cliente, Integer>{
    
}
