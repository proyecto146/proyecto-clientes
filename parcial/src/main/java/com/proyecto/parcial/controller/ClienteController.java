package com.proyecto.parcial.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.proyecto.parcial.model.Cliente;
import com.proyecto.parcial.service.ClienteService;


@RestController
@RequestMapping("/Cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @PostMapping(value = "/guardar")
    private ResponseEntity<Cliente> guardar (@RequestBody Cliente cliente){
        Cliente temporal = clienteService.create(cliente);

        try {
            return ResponseEntity.created(new URI("/Cliente"+temporal.getId())).body(temporal);

        }catch(Exception e){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }

    }

    @GetMapping
    private ResponseEntity<List<Cliente>> listarClientes (){
        return ResponseEntity.ok(clienteService.getAllCliente());
    }

    @DeleteMapping("/eliminar/{id}")
    public String deleteById(@PathVariable("id") Integer id) {
       return clienteService.deleteCliente(id);
    }

    @GetMapping  (value  = "/{id}")
    private ResponseEntity<Optional<Cliente>> listarClientesId (@PathVariable ("id") Integer id){
        return ResponseEntity.ok(clienteService.findById(id));
    }


    @PutMapping (value = "/actualizar")
    public Cliente actualizarCliente(@RequestBody Cliente cli ) {
        
       return clienteService.update(cli);
    }
}

