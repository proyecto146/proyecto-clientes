package com.proyecto.parcial.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;


@Data
@Entity
@Table(name="CLIENTES" , schema="dbo")
public class Cliente {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_cli")
    private Integer id;

    @Column(name="nombre_cli")
    private String nombre;

    @Column(name="apellido_cli")
    private String apellido;

    @Column(name="telefono_cli")
    private Integer telefono;

    @Column(name="cedula_cli")
    private Integer cedula;

}
